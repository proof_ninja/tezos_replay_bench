## Usage

```console
scripts/main.sh <tezos node dir> <shapshots dir> <tezos repository url> <tezos version or commit hash> <level1> <level2>
```
