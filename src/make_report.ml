let catch f =
  try Ok (f ()) with
  | e -> Error e

let dmap default f res =
  Option.map f res
  |> Option.value ~default

let (!%) f = Printf.sprintf f

let read_file_with filename f =
  let ch = open_in filename in
  match catch (fun () -> f ch) with
  | Ok y ->
    close_in ch; y
  | Error e -> close_in ch; raise e

let write_file_with filename f =
  let ch = open_out filename in
  match catch (fun () -> f ch) with
  | Ok y ->
    close_out ch; y
  | Error e -> close_out ch; raise e

let read_all filename =
  let rec iter store ch =
    match catch @@ fun () -> input_line ch with
    | Ok line -> iter (line :: store) ch
    | Error End_of_file -> List.rev store
    | Error e -> raise e
  in
  read_file_with filename (fun ch ->
      iter [] ch
    )

let write_lines filename lines =
  write_file_with filename (fun ch ->
      List.iter (fun line -> output_string ch (line ^ "\n")) lines)

module Report = struct
  type t = {
    date: string option;
    command: string option;
    total_time_sec : float option;
    percentiles: string list option;
    context_size_kb_before: int option;
    context_size_kb_after: int option;
  }
end

type report = Report.t

let get_lines dir filename =
  (catch @@ fun () -> read_all (dir ^ filename))
  |> Result.to_option

let get_1line dir filename =
  get_lines dir filename
  |> Option.map List.hd

let get_date dir =
  get_1line dir "/date"

let get_command dir =
  get_1line dir "/command"

let get_total_time_sec dir =
  get_1line dir "/time_sec"
  |> Option.map float_of_string

let get_percentiles dir =
  match catch @@ fun () -> read_all (dir ^ "/percentiles.md") with
  | Ok lines -> Some lines
  | Error _ -> None

let get_context_size_before dir =
  match catch @@ fun () -> read_all (dir ^ "/context_size_kb_before") with
  | Ok lines ->
    Some (int_of_string @@ String.concat "" lines)
  | Error _ -> None

let get_context_size_after dir =
  match catch @@ fun () -> read_all (dir ^ "/context_size_kb_after") with
  | Ok lines ->
    Some (int_of_string @@ String.concat "" lines)
  | Error _ -> None

let get_report dir =
  let date = get_date dir in
  let command = get_command dir in
  let total_time_sec = get_total_time_sec dir in
  let percentiles = get_percentiles dir in
  let context_size_kb_before = get_context_size_before dir in
  let context_size_kb_after = get_context_size_after dir in
  Report.{ date; command; total_time_sec; percentiles; context_size_kb_before; context_size_kb_after}

let get_template () =
  let template_file = "./src/report.md.template" in
  let template = read_all template_file in
  template

let create_markdown report =
  let template = get_template () in
  let f =
    let pos_date = Re.(compile @@ str "${DATE}") in
    let pos_command = Re.(compile @@ str "${COMMAND}") in
    let pos_total_time_sec = Re.(compile @@ str "${TOTAL_TIME}") in
    let pos_percentiles = Re.(compile @@ str "${SPEED_PERCENTILES}") in
    let pos_context_size_before = Re.(compile @@ str "${CONTEXT_SIZE_BEFORE}") in
    let pos_context_size_after = Re.(compile @@ str "${CONTEXT_SIZE_AFTER}") in
    fun line ->
      line
      |> Re.replace_string pos_date ~by:Report.(dmap "NONE" !%"%s" report.date)
      |> Re.replace_string pos_command ~by:Report.(dmap "NONE" !%"%s" report.command)
      |> Re.replace_string pos_total_time_sec ~by:Report.(dmap "NONE" !%"%f" report.total_time_sec)
      |> Re.replace_string pos_percentiles ~by:Report.(dmap "NONE" (String.concat "\n") report.percentiles)
      |> Re.replace_string pos_context_size_before ~by:Report.(dmap "NONE" !%"%d" report.context_size_kb_before)
      |> Re.replace_string pos_context_size_after ~by:Report.(dmap "NONE" !%"%d" report.context_size_kb_after)
  in
  List.map f template

let () =
  let report_dir = Sys.argv.(1) in
  if Sys.is_directory report_dir = false then failwith ("no such directory: "^report_dir) else
  let report = get_report report_dir in
  let output_md = report_dir ^ "/report.md" in
  create_markdown report
  |> write_lines output_md
