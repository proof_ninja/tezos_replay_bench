set datafile separator ","

set term svg size 640, 480

set xtics rotate by 270
set xlabel 'Block Level'
set format x "%.3sx10^{%T}"

filename = report_dir."/replays.csv"

# output overlayed multiplot 
set output report_dir."/time_disk_gc_multiplot_overlay.svg"
set multiplot layout 1,1
set size ratio 1

set ytics auto
set y2tics auto
set ytics nomirror
set ylabel ''
set y2label ''
set format y ""
set format y2 ""

plot \
 	filename using 1:8 axis x1y1 with lines linecolor rgb "orange" title "", \
 	filename using 1:9 axis x1y2 with lines linecolor rgb "dark-orange" title ""

plot \
	filename using 1:($5*1000*1000) axis x1y1 with lines linecolor rgb "dark-violet" title "", \
 	filename using 1:2 axis x1y2 with lines linecolor rgb "violet" title ""
 
plot \
 	filename using 1:6 axis x1y1 with lines linecolor rgb "green" title "", \
 	filename using 1:7 axis x1y2 with lines linecolor rgb "dark-green" title ""


# output multiplot 
unset multiplot
set term svg size 640, 1440
set output report_dir."/time_disk_gc_multiplot.svg"
set multiplot layout 3,1
set size ratio 1

set y2tics
set ytics nomirror
set ylabel 'Context size (KiB)'
set y2label 'Block validation time[ms]'
set format y "%.2sx10^{%T}"
set format y2 "%.2sx10^{%T}"
plot \
	filename using 1:($5*1000*1000) axis x1y1 with lines linecolor rgb "dark-violet" title "disk size", \
 	filename using 1:2 axis x1y2 with lines linecolor rgb "violet" title "validation time"

set ytics
set y2tics nomirror
set ylabel 'Number of Minor\_collection'
set y2label 'Number of Major\_collection'
set format y "%.2sx10^{%T}"
set format y2 "%.2sx10^{%T}"
plot \
 	filename using 1:6 axis x1y1 with lines linecolor rgb "green" title "minor", \
 	filename using 1:7 axis x1y2 with lines linecolor rgb "dark-green" title "major"

set ylabel 'Number of Compaction'
set y2label 'Number of Forced\_major\_collection'
set ytics
set y2tics nomirror
set format y "%.2sx10^{%T}"
set format y2 "%.2sx10^{%T}"
plot \
 	filename using 1:8 axis x1y1 with lines linecolor rgb "orange" title "compaction", \
 	filename using 1:9 axis x1y2 with lines linecolor rgb "dark-orange" title "forced MC"

