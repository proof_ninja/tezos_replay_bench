#!/bin/bash
set -u
REPORT_DIR=$1
OUTPUT=$REPORT_DIR/rss_kb.csv
while true
do
    PID=$(pidof "tezos-node")
    if [ ! -z $PID ] ; then
        RSS=$(ps --pid $PID --format "rss,vsz,pmem" --no-headers)
        DATETIME=$(date +"%Y/%m/%d %T")
        echo "$DATETIME,$RSS" >> $OUTPUT
    fi
    sleep 1
done
