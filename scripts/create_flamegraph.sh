#!/bin/bash
set -eux

## parameters
ARCHIVE_DIR=$1
TEZOS_COMMIT_HASH=$2
LEVEL1=$3
LEVEL2=$4

DIR=$(dirname $(dirname $0))
TARGET_BLOCKS=$(seq $((LEVEL1 + 1)) $LEVEL2)
TEZOS_SRC_DIR=$DIR/tezos-$TEZOS_COMMIT_HASH
REPORT_DIR=$DIR/reports/report-$TEZOS_COMMIT_HASH-$LEVEL1-$LEVEL2
REPLAY_DIR=replaydir-$LEVEL1
LOG_OUTPUT=$REPORT_DIR/node.log
FLAMEGRAPH_DIR=$DIR/FlameGraph
PERF_DATA=$DIR/perf-$TEZOS_COMMIT_HASH-$LEVEL1-$LEVEL2.data
OUTPUT_SVG=$DIR/flamegraph-$TEZOS_COMMIT_HASH-$LEVEL1-$LEVEL2.svg

if !(type "perf" > /dev/null 2>&1); then
    echo "perf command not founds"
    exit 1
fi

## build tezos static version
cd $TEZOS_SRC_DIR
make clean
eval $(opam config env)
dune build $(for i in src/{,proto_*/}bin_* ; do echo @$i/install ; done) --profile static
install -t . _build/install/default/bin/*

## get flame graph scripts
if [ ! -e $FLAMEGRAPH_DIR ]; then
    git clone $DIR/
fi

## take perf data
PERF="perf record --output $PERF_DATA -F 997 -g --call-graph=dwarf"
$PERF -- ./tezos-binaries/tezos-node replay --log-output=$LOG_OUTPUT --data-dir $REPLAY_DIR $TARGET_BLOCKS

## create flame graph
perf script --input $PERF_DATA | $FLAMEGRAPH_DIR/stackcollapse-perf.pl | $FLAMEGRAPH_DIR/flamegraph.pl > $OUTPUT_SVG
